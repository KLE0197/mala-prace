# "Malá" práce

Připravte repozitář na Gitlabu, který bude používat generátor statických stránek.
CI bude generovat HTML stránky z Markdown podkladu.
Součástí CI by měl být také test kvality Markdown souborů.

Požadavky na kvalitu práce (hodnotící kritéria):

* [x] GIT repozitář na Gitlabu
  * [x] Repozitář má README.md
* [x] Použití libovolného generátoru statických stránek dle vlastního výběru
* [x] Vytvořená CI v repozitáři
* [x] CI má minimálně dvě úlohy:
  * [x] Test kvality Markdown stránek
  * [x] Generování HTML stránek z Markdown zdrojů
* [x] CI má automatickou úlohou nasazení web stránek (deploy)
* [x] Gitlab projekt má fukční web stránky s generovaným obsahem na URL:
